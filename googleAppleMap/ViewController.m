//
//  ViewController.m
//  googleAppleMap
//
//  Created by Prince on 14/10/15.
//  Copyright (c) 2015 Prince. All rights reserved.
//

#import "ViewController.h"
#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <GoogleMaps/GoogleMaps.h>
@interface ViewController ()
{
    
    CLLocationManager *manager;
    NSString *storedCityName;
    CLLocationCoordinate2D position1;
    GMSMarker *show1;
    
    NSString *longName;
    int i;
    NSDictionary *locationDict;
}
@property (strong, nonatomic) IBOutlet MKMapView *appleMap;
@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;

@property (strong, nonatomic) IBOutlet UILabel *label1;
@property (strong, nonatomic) IBOutlet UILabel *label2;
@property (strong, nonatomic) IBOutlet UITextField *textField1;
@property (strong, nonatomic) IBOutlet UIButton *button1;

@property (strong, nonatomic) IBOutlet GMSMapView *googleMap12;



//@property (strong, nonatomic) IBOutlet UITableView *tableView1;



@end

@implementation ViewController
@synthesize googleMap12;
@synthesize appleMap;
//@synthesize tableView1;

- (void)viewDidLoad {
    
    manager=[[CLLocationManager alloc]init];
    manager.delegate=self;
    manager.desiredAccuracy=kCLLocationAccuracyBest;
    [manager requestWhenInUseAuthorization];
    [manager startUpdatingLocation];
    
    
    googleMap12.delegate = self;
    
    
    
  //  data=searchData;
   // [tableView1 reloadData];
   // tableView1.hidden=true;
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}
//- (IBAction)getLocationPressed:(id)sender {
//    
//   [ manager startUpdatingLocation];
//}
//    - (void)locationManager:(CLLocationManager *)manager
//didFailWithError:(NSError *)error
//    {
//        NSLog(@"didFailWithError: %@", error);
//        UIAlertView *errorAlert = [[UIAlertView alloc]
//                                   initWithTitle:@"Error" message:@"Failed to Get Your Location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [errorAlert show];
//    }
//    - (void)locationManager:(CLLocationManager *)manager
//didUpdateToLocation:(CLLocation *)newLocation
//fromLocation:(CLLocation *)oldLocation
//    {
//        NSLog(@"didUpdateToLocation: %@", newLocation);
       // currentLocation = newLocation;
//        
//        if (currentLocation != nil) {
//            self.textField1.text = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
//            self.textField2.text= [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
//            
//            
//            
//            CLLocationCoordinate2D position=currentLocation.coordinate;
//            MKPointAnnotation *annotationPoint = [[MKPointAnnotation alloc] init];
//            MKCoordinateRegion region=MKCoordinateRegionMakeWithDistance(currentLocation.coordinate, 500, 500);
//            [self.appleMap setRegion:region animated:YES];            //[self.appleMap setRegion:region];
//            annotationPoint.coordinate = position;
//            annotationPoint.title = [NSString stringWithFormat:@"%@",storedCityName];
//            annotationPoint.subtitle = @"sir";
//            [appleMap addAnnotation:annotationPoint];
//            
//            
//            
//            
//            GMSMarker *show = [[GMSMarker alloc] init];
//            show.position = currentLocation.coordinate;
//            show.title = [NSString stringWithFormat:@"%@",storedCityName];
//            show.snippet = @"";
//            show.map = _googleMap12;
//            
//          
//            [self Location];
//            
//            
//    
//}
//    }
//
//        - (void)Location {       //function created to show the name of corrditae in map
//            
//            CLGeocoder *geocoder = [[CLGeocoder alloc] init];
//            
//            CLLocation *newLocation = [[CLLocation alloc]initWithLatitude:currentLocation.coordinate.latitude
//                                                                longitude:currentLocation.coordinate.longitude];
//            
//            [geocoder reverseGeocodeLocation:newLocation
//                           completionHandler:^(NSArray *placemarks, NSError *error) {
//                               
//                               if (error) {
//                                   NSLog(@"Geocode failed with error: %@", error);
//                                   return;
//                               }
//                               
//                               if (placemarks && placemarks.count > 0)
//                               {
//                                   CLPlacemark *placemark = placemarks[0];
//                                   
//                                   NSDictionary *addressDictionary =
//                                   placemark.addressDictionary;
//                                   NSLog(@"%@",addressDictionary);
//                                 
//                                   
//                                   
//                                   storedCityName=addressDictionary[@"City"];
//                                   
//                               }
//                               
//                           }];
//
//            
//        }
//
//



//- (void)mapView:(GMSMapView *)mapView
//didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
//
//
//
//{
//    
//    
////    CLLocationCoordinate2D position=currentLocation.coordinate;
////    MKPointAnnotation *annotationPoint = [[MKPointAnnotation alloc] init];
////    MKCoordinateRegion region=MKCoordinateRegionMakeWithDistance(currentLocation.coordinate, 500, 500);
////    [self.appleMap setRegion:region animated:YES];            //[self.appleMap setRegion:region];
////    annotationPoint.coordinate = position;
////    annotationPoint.title = [NSString stringWithFormat:@"%@",storedCityName];
////    annotationPoint.subtitle = @"sir";
////    [appleMap addAnnotation:annotationPoint];
////    
////    
//    
//    position = coordinate;
//    show1 = [GMSMarker markerWithPosition:position];
//    //show1.position = currentLocation.coordinate;
////    show1.title = [NSString stringWithFormat:@"%@",storedCityName];
////    show1.snippet = @"";
////    show1.map = googleMap12;
//
//    [self fetchLocation];
//    
//
//   // [ manager startUpdatingLocation];
//    
//    
//}

//- (void)fetchLocation {       //function created to show the name of corrditae in map
//    
//    CLGeocoder *geocoder1 = [[CLGeocoder alloc] init];
//    
//    CLLocation *newLocation1 = [[CLLocation alloc]initWithLatitude:position.latitude
//                                                        longitude:position.longitude];
//    
//    [geocoder1 reverseGeocodeLocation:newLocation1
//                   completionHandler:^(NSArray *placemarks, NSError *error) {
//                       
//                       if (error)
//                       {
//                           NSLog(@"Geocode failed with error: %@", error);
//                           return;
//                      }
//                       
//                       if (placemarks && placemarks.count > 0)
//                       {
//                           CLPlacemark *placemark = placemarks[0];
//                           
//                           NSDictionary *addressDictionary1 = placemark.addressDictionary;
//                           
//                           NSLog(@"%@",addressDictionary1);
//                           
//                           storedCityName=addressDictionary1[@"State"];
//                          // NSString *countryName =addressDictionary1[@"Country"];
//                           
//                           show1 = [GMSMarker markerWithPosition:position];
//
//                           //show1.position = position.coordinate;
//                           show1.title = [NSString stringWithFormat:@"%@",storedCityName];
//                           //show1.title=[NSString stringWithFormat:@"%@",countryName];
//                           show1.snippet = @"";
//                           show1.map = googleMap12;
//                           
//                       }
//                       
//                   }];
//    
//    
//}
//

 - (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar

{

    
    NSString *address ;
     address = searchBar.text;
    
    NSString *urlString = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/geocode/json?address=%@&key=AIzaSyA-itSW_zSkkwtCmMaHJ0c94oGF7qI7Xb4",address];
    NSURL *url=[NSURL URLWithString:urlString];
    NSData *data = [NSData dataWithContentsOfURL:url];
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
    
    // NSLog(@"%@",json);
    
    
    NSArray *resultArray=json[@"results"];
    NSLog(@"%@",resultArray);
    for (i=0; i<resultArray.count; i++) {
        
        NSDictionary *adressCompontDict=resultArray[i];
       // NSLog(@"%@",resultArray);
        
        if (adressCompontDict[@"address_components"] != NULL)
        {

            NSArray * addressComponentArray = adressCompontDict[@"address_components"];
            
            for (int j = 0; j<addressComponentArray.count ; j++) {
                
               // NSLog(@"%@",addressComponentArray);
                
                NSDictionary *longNameDict=addressComponentArray[i];
                if (longNameDict [@"long_name"]) {
                    longName=longNameDict[@"long_name"];
                    //NSLog(@"%@",longName);
                }
                    NSDictionary *formtAddressDict=resultArray[i];
                NSLog(@"%@",formtAddressDict);
                    
                    if (formtAddressDict[@"formatted_address"] != NULL)
                    {
                        NSDictionary *geoDict=formtAddressDict[@"geometry"];
                        NSLog(@"%@",geoDict);
                        
                        NSDictionary *BoundDict=geoDict[@"bounds"];
                        NSLog(@"%@",BoundDict);
                        
                        
                        NSDictionary *locationDict=geoDict[@"location"];
                       // NSString *longitudeLatitude=[NSString stringWithFormat:@"%@",locationDict];
                        NSLog(@"%@",locationDict);
                        
                        
                      
                    }

                    
          
       

                    show1 = [GMSMarker markerWithPosition:position1];
                    show1.title = [NSString stringWithFormat:@"%@",longNameDict];
                    //show1.snippet = @"";
                  show1.map = googleMap12;
                }
            
            }
            
            
            
        }
    }



        
        




        
        //NSLog(@"%@",adressCompontDict);
        

//
//    NSArray *adressCmponentArray=resultArray[@"address_components"];
//    for (i=0; i<=adressCmponentArray.count; i++)
//    {
//         NSLog(@"%@",adressCmponentArray);
//    }
//   
//
//    
    



//
//- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    return 1;
//}
//- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
//{
//    return 4;
//}
//
//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"identity"];
//    //cell.textLabel.text=searchResult[indexPath.row];
//    return cell;
//}
//
//- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
//{
//    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@", searchText];
//    
//    NSArray *results = [data filteredArrayUsingPredicate:resultPredicate];

    
   // if (searchResult.count==0) {
        
       // searchResult=[NSArray arrayWithArray:data];
        
    
  //  [tableView1 reloadData];
//}

    
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
